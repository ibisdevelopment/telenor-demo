module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      borderRadius: {
        '4xl': '2rem'
      },
      colors: {
        'telenor-blue': '#00B7EE',
      },
      width: {
        '88': '22rem'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
